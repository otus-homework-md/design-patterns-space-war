﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UserObjects.Interfaces;
using UserObjects.SubObjects;

namespace UserObjects.Adapters;

public class MovableObject : IMovable
{
    IUserObject UserObject;

    public MovableObject(IUserObject userObject)
    {
        UserObject = userObject;
    }

    public Vector<int> GetPosition()
    {
        return (Vector<int>)UserObject.GetProperty("Position");
    }

    public Vector<int> GetVelocity()
    {
        int d = ((Direction)UserObject.GetProperty("Direction")).CurrentDirection;
        int n = (int)UserObject.GetProperty("DirectionsNumber");
        int v = (int)UserObject.GetProperty("Velocity");

        return new Vector<int>(new int[8] { (int)(v * Math.Round(Math.Cos(2 * Math.PI * d / n))), (int)(v * Math.Round(Math.Sin(2 * Math.PI * d / n))), 0, 0, 0, 0, 0, 0 });

    }

    public void SetPosition(Vector<int> newVector)
    {
        UserObject.SetProperty("Position", newVector);
    }
}
