﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UserObjects.Interfaces;

namespace UserObjects.Adapters;

public class RotatableObject : IRotatable
{
    IUserObject UserObject;

    public RotatableObject(IUserObject userObject)
    {
        UserObject = userObject;
    }
    public int GetAngularVelocity()
    {
        return (int)UserObject.GetProperty("AngularVelocity");
    }

    public IDirection GetDirection()
    {
        return (IDirection)UserObject.GetProperty("Direction");
    }

    public void SetDirection(IDirection direction)
    {
        UserObject.SetProperty("Direction", direction);
    }
}
