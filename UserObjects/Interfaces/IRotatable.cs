﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserObjects.Interfaces;
/// <summary>
/// Rotation interface
/// </summary>
public interface IRotatable
{
    IDirection GetDirection();
    int GetAngularVelocity();
    void SetDirection(IDirection direction);
}
