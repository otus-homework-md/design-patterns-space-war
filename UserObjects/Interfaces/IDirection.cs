﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserObjects.Interfaces;

public interface IDirection
{
    int CurrentDirection { get; }
    int DirectionsNumber { get; }
    IDirection Next(int numberOfSkippedPositions);
}
