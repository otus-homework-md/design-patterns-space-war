﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace UserObjects.Interfaces;
/// <summary>
/// Translation interface
/// </summary>
public interface IMovable
{
    /// <summary>
    /// Get current position
    /// </summary>
    /// <returns>Current position coordinate</returns>
    Vector<int> GetPosition();
    /// <summary>
    /// Get current velocity of translation movement
    /// </summary>
    /// <returns>Current velocity</returns>
    Vector<int> GetVelocity();
    /// <summary>
    /// Set current position 
    /// </summary>
    /// <param name="newVector"></param>
    void SetPosition(Vector<int> newVector);
}
