﻿using System.Numerics;
using System.Reflection;
using UserObjects.Interfaces;
using UserObjects.SubObjects;

namespace UserObjects.Objects;

public class UserObject : IUserObject
{
    public virtual Vector<int> Position { get; set; }
    public IDirection Direction { get; set; }
    public int DirectionsNumber { get; set; }
    public virtual int Velocity { get; set; }
    public virtual int AngularVelocity { get; set; }

    public UserObject()
    {
        Position = Vector<int>.Zero;

        Direction = new Direction(8, 0);
        DirectionsNumber = Direction.DirectionsNumber;
        Velocity = 0;
        AngularVelocity = 0;
    }

    public object GetProperty(string name)
    {
        PropertyInfo? propertyInfo = typeof(UserObject).GetProperty(name);
        if (propertyInfo != null) return propertyInfo.GetValue(this);
        else throw new KeyNotFoundException("Property dose not exist");
    }

    public void SetProperty(string name, object value)
    {
        PropertyInfo? propertyInfo = typeof(UserObject).GetProperty(name);
        if (propertyInfo != null)
        {
            propertyInfo.SetValue(this, value);
        }
        else
        {
            throw new KeyNotFoundException("Property dose not exist");
        }

    }
}